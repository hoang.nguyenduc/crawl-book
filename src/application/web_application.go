package application

import (
	"github.com/labstack/echo/v4"
	"gopkg.in/labstack/echo.v4/middleware"
)

var WebApplication *echo.Echo

func init() {
	WebApplication = echo.New()
	WebApplication.Use(middleware.Logger())
	WebApplication.Use(middleware.Recover())
}
func GetWebAplicantion() *echo.Echo {
	return WebApplication
}
