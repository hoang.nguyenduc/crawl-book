package database

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

func Connect() (db *gorm.DB, err error) {
	return gorm.Open("mysql", "root:12345678@/crawl?charset=utf8mb4&parseTime=True")
}
func Close(db *gorm.DB) {
	db.Close()
}
