package models

import "time"

type PublicationStatus int

const (
	PENDING   = 0
	COMPLETED = 1
)

type Publication struct {
	Id             int
	Name           string `gorm:"type:varchar(32);unique_index`
	Thumbnail      string
	LargeThumbnail string
	LocalAuthorId  int
	Url            string `gorm:"type:varchar(255); not null`
	Description    string `gorm:"type:varchar(255);`
	LastSync       *time.Time
	State          SyncState
	Status         PublicationStatus
	CurrentPage    int
}

func (Publication) TableName() string {
	return "publication"
}
