package models

type Chapter struct {
	Id            int
	PublicationId int
	ChapterIndex  int
	Name          string
	Content       string
	Url           string
}

func (Chapter) TableName() string {
	return "chapter"
}
