package models

type PubLocalCategory struct {
	Id              int
	PublicationId   int
	LocalCategoryId int
}

func (PubLocalCategory) TableName() string {
	return "pub_local_category"
}
