package models

import "time"

type SyncState int

const (
	Free     SyncState = 0
	Excuting SyncState = 1
)

type LocalCategory struct {
	// gorm.Model
	Id          int
	Name        string `gorm:"type:varchar(32);unique_index`
	Url         string `gorm:"type:varchar(255); not null`
	Description string `gorm:"type:varchar(255);`
	ServerID    int
	Server      Server `gorm:"foreignkey:ServerID`
	LastSync    *time.Time
	State       SyncState
	CurrentPage int
}

func (LocalCategory) TableName() string {
	return "local_category"
}
