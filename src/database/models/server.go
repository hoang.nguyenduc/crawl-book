package models

import "github.com/jinzhu/gorm"

type Server struct {
	gorm.Model
	Name        string `gorm:"type:varchar(32);unique_index`
	Url         string `gorm:"type:varchar(255); not null`
	Description string `gorm:"type:varchar(255);`
}
