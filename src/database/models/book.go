package models

import "time"

type Book struct {
	Id                    int
	Name                  string `gorm:"type:varchar(32);unique_index`
	Thumbnail             string
	AuthorId              int
	OfficialPublicationId int
	Url                   string `gorm:"type:varchar(255); not null`
	Description           string `gorm:"type:varchar(255);`
	LastSync              *time.Time
	Status                PublicationStatus
}

func (Book) TableName() string {
	return "book"
}
