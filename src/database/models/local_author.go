package models

type LocalAuthor struct {
	Id          int
	Name        string
	Url         string `gorm:"type:varchar(255); not null`
	Description string
}

func (LocalAuthor) TableName() string {
	return "local_author"
}
