CREATE TABLE IF NOT EXISTS pub_local_category (
  id int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  publication_id int(11),
  FOREIGN KEY (publication_id) REFERENCES publication(id),
  local_category_id int(11),
  FOREIGN KEY (local_category_id) REFERENCES local_category(id),
  created_at DATETIME DEFAULT CURRENT_TIMESTAMP, 
  updated_at DATETIME ON UPDATE CURRENT_TIMESTAMP
);
