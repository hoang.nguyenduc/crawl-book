CREATE TABLE IF NOT EXISTS book (
  id int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(255),
  thumbnail VARCHAR(1024),
  author_id int(11) NOT NULL,
  FOREIGN KEY (author_id) REFERENCES author(id),
  official_publication_id int(11),
  FOREIGN KEY (official_publication_id) REFERENCES publication(id),
  url VARCHAR(255),
  description TEXT,
  status int(11) NOT NULL DEFAULT 0,
  created_at DATETIME DEFAULT CURRENT_TIMESTAMP, 
  updated_at DATETIME ON UPDATE CURRENT_TIMESTAMP
);
