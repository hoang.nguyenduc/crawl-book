CREATE TABLE IF NOT EXISTS book_category (
  id int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  book_id int(11),
  FOREIGN KEY (book_id) REFERENCES book(id),
  category_id int(11),
  FOREIGN KEY (category_id) REFERENCES category(id),
  created_at DATETIME DEFAULT CURRENT_TIMESTAMP, 
  updated_at DATETIME ON UPDATE CURRENT_TIMESTAMP
);
