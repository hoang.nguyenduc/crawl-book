CREATE TABLE IF NOT EXISTS server (
  id int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(16),
  url VARCHAR(100),
  description VARCHAR(255),
  created_at DATETIME DEFAULT CURRENT_TIMESTAMP, 
  updated_at DATETIME ON UPDATE CURRENT_TIMESTAMP
);
INSERT INTO server (name, url, description)
VALUES ('truyen_full', 'https://truyenfull.vn/', '');
