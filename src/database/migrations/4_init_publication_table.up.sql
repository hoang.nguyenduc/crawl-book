CREATE TABLE IF NOT EXISTS publication (
  id int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(255),
  thumbnail VARCHAR(1024),
  local_author_id int(11) NOT NULL,
  FOREIGN KEY (local_author_id) REFERENCES local_author(id),
  url VARCHAR(255),
  description VARCHAR(1024),
  last_sync DATETIME,
  current_page int(11) NOT NULL DEFAULT 0,
  state int(11) NOT NULL DEFAULT 0,
  created_at DATETIME DEFAULT CURRENT_TIMESTAMP, 
  updated_at DATETIME ON UPDATE CURRENT_TIMESTAMP
);
