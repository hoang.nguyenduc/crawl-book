CREATE TABLE IF NOT EXISTS chapter (
  id int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  publication_id int(11) NOT NULL,
  FOREIGN KEY (publication_id) REFERENCES publication(id),
  chapter_index int(11) NOT NULL,
  name VARCHAR(1024),
  content TEXT(1024),
  url VARCHAR(1024),
  created_at DATETIME DEFAULT CURRENT_TIMESTAMP, 
  updated_at DATETIME ON UPDATE CURRENT_TIMESTAMP
);
