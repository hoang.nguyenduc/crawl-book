ALTER TABLE publication
ADD book_id int(11),
ADD CONSTRAINT FOREIGN KEY (book_id) REFERENCES book(id);
