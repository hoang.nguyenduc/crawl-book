package repository

import (
	"crawl/src/database"
	"crawl/src/database/models"
)

func GetBook() []models.Book {
	db, _ := database.Connect()
	defer database.Close(db)
	var books []models.Book
	db.Limit(10).Find(&books)
	return books
}
