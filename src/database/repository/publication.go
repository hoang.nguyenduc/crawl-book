package repository

import (
	"crawl/src/database"
	"crawl/src/database/models"
	"time"
)

func CreatPublication(Name string, Url string, Thumbnail string) models.Publication {
	db, _ := database.Connect()
	defer database.Close(db)
	var publication models.Publication
	db.Where(models.Publication{Url: Url}).Attrs(models.Publication{
		Name:          Name,
		Url:           Url,
		Thumbnail:     Thumbnail,
		LocalAuthorId: 1,
	}).FirstOrCreate(&publication)
	return publication
}

func UpdatePublication(Id int, LargeThumbnail string, AuthorId int, Status models.PublicationStatus, Desc string) models.Publication {
	db, _ := database.Connect()
	defer database.Close(db)
	var publication models.Publication
	db.First(&publication, Id)
	db.Model(&publication).Updates(&models.Publication{
		LargeThumbnail: LargeThumbnail,
		LocalAuthorId:  AuthorId,
		Status:         Status,
		Description:    Desc,
	})
	return publication
}

func AddPublicationToLocalCategory(publicationId int, localCategoryId int) {
	db, _ := database.Connect()
	defer database.Close(db)
	var pubLocalCategory models.PubLocalCategory
	db.Where(models.PubLocalCategory{
		PublicationId:   publicationId,
		LocalCategoryId: localCategoryId,
	}).Attrs(models.PubLocalCategory{
		PublicationId:   publicationId,
		LocalCategoryId: localCategoryId,
	}).FirstOrCreate(&pubLocalCategory)
}

func GetLocalPublicationNotSync() (publication models.Publication, err []error) {
	db, _ := database.Connect()
	defer database.Close(db)
	//  localCategory models.LocalCategory
	currentTime := time.Now().UTC().Add(-1 * time.Hour * time.Duration(1))
	err = db.Where("(last_sync < ? or last_sync is null) and state = 0", currentTime).Order("last_sync ASC").First(&publication).GetErrors()

	return publication, err
}

func SetStatePublicationToExcuting(id int) {
	db, _ := database.Connect()
	defer database.Close(db)
	var publication models.Publication
	db.First(&publication, id)
	publication.State = models.Excuting
	db.Save(publication)
}

func SetStatePublicationToFree(id int) {
	db, _ := database.Connect()
	defer database.Close(db)
	var publication models.Publication
	db.First(&publication, id)
	publication.State = models.Free
	currentTime := time.Now().UTC()
	publication.LastSync = &currentTime

	db.Save(publication)
}
