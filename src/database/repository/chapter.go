package repository

import (
	"crawl/src/database"
	"crawl/src/database/models"
)

func CreatChapter(PublicationId int, Name string, Url string, ChapterIndex int, Content string) models.Chapter {
	db, _ := database.Connect()
	defer database.Close(db)
	var chapter models.Chapter
	db.Where(models.Chapter{Url: Url}).Attrs(models.Chapter{
		Name:          Name,
		PublicationId: PublicationId,
		Url:           Url,
		ChapterIndex:  ChapterIndex,
		Content:       Content,
	}).FirstOrCreate(&chapter)
	return chapter
}
func GetChapterByUrl(Url string) models.Chapter {
	db, _ := database.Connect()
	defer database.Close(db)
	var chapter models.Chapter
	db.Where(models.Chapter{Url: Url}).Attrs(models.Chapter{
		Url: Url,
	}).First(&chapter)
	return chapter
}
