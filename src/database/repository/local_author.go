package repository

import (
	"crawl/src/database"
	"crawl/src/database/models"
)

func CreatLocalAuthor(Name string, Url string) models.LocalAuthor {
	db, _ := database.Connect()
	defer database.Close(db)
	var localAuthor models.LocalAuthor
	db.Where(models.LocalAuthor{Url: Url}).Attrs(models.LocalAuthor{
		Name: Name,
		Url:  Url,
	}).FirstOrCreate(&localAuthor)
	return localAuthor
}
