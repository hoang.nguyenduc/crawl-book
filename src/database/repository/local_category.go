package repository

import (
	"crawl/src/database"
	"crawl/src/database/models"
	"time"
)

func GetLocalCategoryNotSync() (localCategory models.LocalCategory, err []error) {
	db, _ := database.Connect()
	defer database.Close(db)
	//  localCategory models.LocalCategory
	currentTime := time.Now().UTC().Add(-1 * time.Hour * time.Duration(1))
	err = db.Where("(last_sync < ? or last_sync is null) and state = 0", currentTime).First(&localCategory).GetErrors()

	return localCategory, err
}
func SetStateLocalCategoryToExcuting(id int) {
	db, _ := database.Connect()
	defer database.Close(db)
	var localCategory models.LocalCategory
	db.First(&localCategory, id)
	localCategory.State = models.Excuting
	db.Save(localCategory)
}

func SetStateLocalCategoryToFree(id int) {
	db, _ := database.Connect()
	defer database.Close(db)
	var localCategory models.LocalCategory
	db.First(&localCategory, id)
	localCategory.State = models.Free
	currentTime := time.Now().UTC()
	localCategory.LastSync = &currentTime

	db.Save(localCategory)
}
