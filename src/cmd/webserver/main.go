package main

import (
	"crawl/src/application"
	"crawl/src/handler"
)

func main() {
	// Echo instance
	app := application.GetWebAplicantion()
	v1 := app.Group("/api")
	Handler := handler.NewHandler()
	Handler.Register(v1)

	// Start server
	app.Logger.Fatal(app.Start(":3000"))
}
