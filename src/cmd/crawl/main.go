package main

import (
	"crawl/src/database/models"
	"crawl/src/database/repository"
	"crawl/src/framework/crawl"
	"fmt"
	"time"

	_ "github.com/jinzhu/gorm/dialects/mysql"
)

func crawlBookName() {
	for {
		category, errors := repository.GetLocalCategoryNotSync()
		if len(errors) > 0 {
			break
		}
		repository.SetStateLocalCategoryToExcuting(category.Id)
		crawl.Collect(category)
		repository.SetStateLocalCategoryToFree(category.Id)

	}
}

func crawlBookDetail() {
	for {
		pub, errors := repository.GetLocalPublicationNotSync()
		if len(errors) > 0 {
			break
		}
		repository.SetStatePublicationToExcuting(pub.Id)
		crawl.CollectPublicationDetail(pub)
		repository.SetStatePublicationToFree(pub.Id)

	}
}

var totalPub = 0

func crawlChapter() {
	for {
		// fmt.Println("totalPub", totalPub)
		time.Sleep(1000 * time.Millisecond)
		if totalPub > 20 {
			continue
		}
		pub, errors := repository.GetLocalPublicationNotSync()
		if len(errors) > 0 {
			fmt.Println("Not found Publication not sync!")
			continue
		}
		totalPub += 1
		repository.SetStatePublicationToExcuting(pub.Id)
		go processCrawlChapter(pub)
	}
	fmt.Println("end crawlChapter")
}
func processCrawlChapter(pub models.Publication) {
	fmt.Println("processCrawlChapter", pub.Id)
	crawl.CollectListChapter(pub)
	repository.SetStatePublicationToFree(pub.Id)
	totalPub -= 1
}
func main() {
	// crawlBookName()
	// crawlBookDetail()
	// crawlChapter()
	fmt.Println("dkm")
	a := repository.GetChapterByUrl("https://truyenfull.vn/co-dao-kinh-phong/chuong-1/")
	fmt.Println(a)
	fmt.Println("dkm2")
}
