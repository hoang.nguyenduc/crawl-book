package handler

import (
	"crawl/src/database/repository"
	"crawl/src/handler/dto"
	"fmt"

	"github.com/labstack/echo/v4"
)

type Sex int

const (
	Male   Sex = 0
	Female Sex = 1
)

func (h *Handler) Recommend(c echo.Context) error {
	// genderStr := c.FormValue("gender")
	// var gender Sex = Female
	// if genderStr == "male" {
	// 	gender = Male
	// }
	books := repository.GetBook()
	// test := dto.ToBookDtos(books)
	fmt.Println(dto.ToBookDtos(books))
	return c.JSON(200, dto.ToBookDtos(books))

}
