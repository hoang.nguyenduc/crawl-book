package dto

import (
	"crawl/src/database/models"
	"crawl/src/system/util"
	"strconv"
)

type BookDto struct {
	Id                string `json:"id"`
	Author            string `json:"author"`
	Cover             string `json:"cover"`
	ShortIntro        string `json:"shortIntro"`
	Title             string `json:"title"`
	HasCp             bool   `json:"hasCp"`
	IsTop             bool   `json:"isTop"`
	IsSeleted         bool   `json:"isSeleted"`
	ShowCheckBox      bool   `json:"showCheckBox"`
	IsFromSD          bool   `json:"isFromSD"`
	Path              string `json:"path"`
	LatelyFollower    int    `json:"latelyFollower"`
	RetentionRatio    int    `json:"retentionRatio"`
	Updated           string `json:"updated"`
	ChaptersCount     int    `json:"chaptersCount"`
	LastChapter       string `json:"lastChapter"`
	RecentReadingTime string `json:"recentReadingTime"`
}

func ToBookDto(m models.Book) BookDto {
	return BookDto{
		Id:                strconv.FormatInt(int64(m.Id), 10),
		Author:            strconv.FormatInt(int64(m.AuthorId), 10),
		Cover:             m.Thumbnail,
		ShortIntro:        m.Description,
		Title:             m.Name,
		HasCp:             true,
		IsTop:             true,
		IsSeleted:         false,
		ShowCheckBox:      false,
		IsFromSD:          false,
		Path:              "",
		LatelyFollower:    0,
		RetentionRatio:    0,
		Updated:           "",
		ChaptersCount:     0,
		LastChapter:       "",
		RecentReadingTime: "",
	}
}
func ToBookDtos(ms []models.Book) []BookDto {
	dtos := util.Map(ms, ToBookDto)
	return dtos.([]BookDto)
}

// util.Map(books, func(x models.Book) models.Book {
// 	return x
// })
