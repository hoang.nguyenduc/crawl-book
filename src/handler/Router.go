package handler

import "github.com/labstack/echo/v4"

func (h *Handler) Register(v1 *echo.Group) {
	// Routes
	book := v1.Group("/book")
	book.GET("/recommend", h.Recommend)

}
