package crawl

import (
	"crawl/src/database/models"
	"crawl/src/database/repository"
	"fmt"
	"strings"

	"github.com/gocolly/colly"
)

func CollectListChapter(pub models.Publication) {
	var nextPage string = pub.Url
	var index = 1
	index = collectSingleListChapter(nextPage, pub.Id, index)
	for {
		nextPage = GetNextPageListChapter(nextPage)
		if nextPage == "" {
			break
		}
		index = collectSingleListChapter(nextPage, pub.Id, index)
	}

}
func collectSingleListChapter(url string, pubId int, index int) int {
	fmt.Println("[book]", url, index)
	c := colly.NewCollector()

	// Find and visit all links
	c.OnHTML("#list-chapter>.row li a", func(e *colly.HTMLElement) {
		url := e.DOM.AttrOr("href", "")
		_title := e.DOM.Text()
		content := getChapterContent(url)
		title := convertTitle(_title)
		fmt.Println("[chapter]", url, title, index)
		repository.CreatChapter(pubId, title, url, index, content)
		index = index + 1
		// handler.AddPublicationToLocalCategory(publication.Id, pubId)

	})

	c.Visit(url)
	return index
}
func convertTitle(_title string) string {
	arr := strings.Split(_title, ":")
	if len(arr) > 1 && len(arr[1]) > 0 {
		return strings.TrimSpace(arr[1])
	}
	return strings.TrimSpace(_title)
}
func getChapterContent(url string) string {
	c := colly.NewCollector()
	var content string = ""

	c.OnHTML(".chapter-c", func(e *colly.HTMLElement) {
		content, _ = e.DOM.Html()
	})

	c.Visit(url)
	return content
}
func GetNextPageListChapter(url string) string {
	c := colly.NewCollector()
	var nextPage string = ""

	c.OnHTML(".pagination-sm", func(e *colly.HTMLElement) {
		lastItem := e.DOM.Find("li").Has(".glyphicon-menu-right")
		nextPage, _ = lastItem.Find("a[href]").Attr("href")
	})

	c.Visit(url)
	return nextPage

}
