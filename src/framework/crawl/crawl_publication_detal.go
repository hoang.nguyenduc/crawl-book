package crawl

import (
	"crawl/src/database/models"
	"crawl/src/database/repository"
	"fmt"

	"github.com/gocolly/colly"
)

func CollectPublicationDetail(publication models.Publication) {
	fmt.Println("CollectPublicationDetail", publication.Url)
	c := colly.NewCollector()

	// Find and visit all links
	c.OnHTML(".col-info-desc", func(e *colly.HTMLElement) {
		url := e.DOM.Find(".books>.book img").AttrOr("src", "")
		desc := e.DOM.Find(".desc-text").Text()
		info := e.DOM.Find(".info>div")

		author := info.First().Find("a").Text()
		authorUrl := info.First().Find("a").AttrOr("href", "")
		statusString := info.First().Next().Next().Next().Find("span").Text()
		var status models.PublicationStatus = models.COMPLETED
		if statusString == "Đang ra" {
			status = models.PENDING
		}
		authorModel := repository.CreatLocalAuthor(author, authorUrl)
		repository.UpdatePublication(publication.Id, url, authorModel.Id, status, desc)
	})

	c.Visit(publication.Url)
}
