package crawl

import (
	"crawl/src/database/models"
	"crawl/src/database/repository"
	"fmt"

	"github.com/gocolly/colly"
)

func Collect(localCategory models.LocalCategory) {
	var nextPage string = localCategory.Url
	collectSinglePage(nextPage, localCategory.Id)
	for {
		nextPage = GetNextPage(nextPage)
		if nextPage == "" {
			break
		}
		collectSinglePage(nextPage, localCategory.Id)
	}

}
func collectSinglePage(url string, localCategoryId int) {
	fmt.Println("collectSinglePage", url)
	c := colly.NewCollector()

	// Find and visit all links
	c.OnHTML(".col-truyen-main .list-truyen .row", func(e *colly.HTMLElement) {
		url := e.DOM.Find(".col-xs-7 a").AttrOr("href", "")
		title := e.DOM.Find(".truyen-title").Text()
		thumbnail := e.DOM.Find(".col-xs-3>div>div[data-desk-image]").AttrOr("data-desk-image", "")
		publication := repository.CreatPublication(title, url, thumbnail)
		repository.AddPublicationToLocalCategory(publication.Id, localCategoryId)

	})

	c.Visit(url)
}
func GetNextPage(url string) string {
	c := colly.NewCollector()
	var nextPage string = ""

	c.OnHTML(".pagination-sm", func(e *colly.HTMLElement) {
		lastItem := e.DOM.Find("li").Has(".glyphicon-menu-right")
		nextPage, _ = lastItem.Find("a[href]").Attr("href")
	})

	c.Visit(url)
	return nextPage

}
